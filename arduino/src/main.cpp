#include <Arduino.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void displayText(Adafruit_SSD1306 *display, String text, int size);

#include <FastLED.h>
#define NUM_LEDS 4
#define DATA_PIN 15
CRGB leds[NUM_LEDS];

#include <SoftwareSerial.h>
#define RX_PIN 13
#define TX_PIN 12
SoftwareSerial mySerial(RX_PIN, TX_PIN);

#include "MHZ19.h"
#define BAUDRATE 9600
MHZ19 co2Sensor;
unsigned long lastDataRetrievedTime = 0;

#include <Bounce2.h>
#define B_1 16
#define B_2 14
Bounce2::Button button1 = Bounce2::Button();

#include <ESP8266WiFi.h>

void setup() {
    Serial.begin(115200);
    WiFi.mode(WIFI_OFF);

    FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
    for(int led = 0; led < NUM_LEDS; led++) {
        leds[led] = CRGB::Black;
    }

    display.setRotation(2);
    if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
      Serial.println(F("SSD1306 allocation failed"));
      for(;;); // Don't proceed, loop forever
    }
    
    delay(1000);

    mySerial.begin(BAUDRATE);
    co2Sensor.begin(mySerial);
    co2Sensor.autoCalibration(false);

    button1.attach( B_1, INPUT );
    button1.interval(5); 
    button1.setPressedState(LOW); 
}

typedef enum { INIT, RUNNING, CALIBRATION_INIT, CALIBRATION_START, CALIBRATION_RUNNING, CALIBRATION_DONE } State;
State state = INIT;
int last_state_change = millis();

const int CALIBRATION_DURATION = 20 * 60 * 1000;
const int CALIBRATION_START_DURATION = CALIBRATION_DURATION;
const int INIT_DURATION = 20 * 1000;

void light_leds(CRGB* leds, int nbLeds, int co2);

void loop()
{
    button1.update();
    String text = "";
    int duration = millis() - last_state_change;
    switch (state)
    {
    case INIT:
        displayText(&display, String((INIT_DURATION - duration) / 1000), 4);
    
        if(duration > INIT_DURATION ) {
            state = RUNNING;
            last_state_change = millis();
        }
        break;

    case RUNNING:
        if (millis() - lastDataRetrievedTime >= 2000)
        {
            int CO2; 

            CO2 = co2Sensor.getCO2();
            displayText(&display, String(CO2), 4);
            lastDataRetrievedTime = millis();

            light_leds(leds, NUM_LEDS, CO2);
        }

        if ( button1.pressed() ) {            
            state = CALIBRATION_INIT;
            last_state_change = millis();
            displayText(&display, "Press again to start calibration", 2);
        }
        break;

    case CALIBRATION_INIT:
        if(button1.pressed()) {
            state = CALIBRATION_START;
            last_state_change = millis();
        } else {
            if(duration > 2000) {
                state = RUNNING;
                last_state_change = millis();
                displayText(&display, "Calibration aborted", 2);
                delay(1000);
            }
        }
        break;

    case CALIBRATION_START:
        for(int i=0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Purple;
        }
        text = "Calibration will \nstart in ";
        text += String((CALIBRATION_START_DURATION - (duration))/1000);
        text += "\nLet in ventilated place";
        displayText(&display, text, 1);

        if(duration > CALIBRATION_DURATION) {
            state = CALIBRATION_RUNNING;
            last_state_change = millis();
        }
        break;

    
    case CALIBRATION_RUNNING:
        for(int i=0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Yellow;
        }
        text = "Calibration in progress. ";
        text += String((CALIBRATION_DURATION - (duration))/1000);

        displayText(&display, text, 1);

        if(duration > CALIBRATION_DURATION) {
            co2Sensor.calibrate();
            state = CALIBRATION_DONE;
            last_state_change = millis();
        }
        break;

    case CALIBRATION_DONE:
        displayText(&display, "Calibration done. Press button to finish.", 1);
        for(int i=0; i < NUM_LEDS; i++) {
            leds[i] = CRGB::Blue;
        }
        if(button1.pressed()) {
            state = RUNNING;
            last_state_change = millis();
        }
        break;

    default:
        break;
    }

    FastLED.show();
}

void displayText(Adafruit_SSD1306 *display, String text, int size) {
    Serial.println(text);
    
    display->clearDisplay();
    display->setTextSize(size);
    display->setTextColor(SSD1306_WHITE);
    display->setCursor(0,0);
    display->println(text);
    display->display();
}

void light_leds(CRGB* leds, int nbLeds, int co2) {
    if(co2 < 800) {
        for(int i=0; i < nbLeds; i++) {
            leds[i] = CRGB::Green;
        }
    }

    if(co2 >= 800) {
        for(int i=0; i < nbLeds; i++) {
            leds[i] = CRGB::Orange;
        }
    }

    if(co2 > 1000) {
        for(int i=0; i < nbLeds; i++) {
            leds[i] = CRGB::Red;
        }
    }
}
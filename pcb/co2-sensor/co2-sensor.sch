EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "CO2 Sensor - MH Z19c"
Date ""
Rev ""
Comp "Crafting Labs"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L crafting_labs:ESP12 U4
U 1 1 61D84145
P 6850 2550
F 0 "U4" H 6675 4437 60  0000 C CNN
F 1 "ESP12" H 6675 4331 60  0000 C CNN
F 2 "crafting-labs:ESP12-breakout_board" H 6850 2550 60  0001 C CNN
F 3 "" H 6850 2550 60  0000 C CNN
	1    6850 2550
	1    0    0    -1  
$EndComp
$Comp
L crafting_labs:HT7333 U1
U 1 1 61D846BD
P 4950 3250
F 0 "U1" H 4633 4237 60  0000 C CNN
F 1 "HT7333" H 4633 4131 60  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Wide" H 4950 3250 60  0001 C CNN
F 3 "" H 4950 3250 60  0000 C CNN
	1    4950 3250
	1    0    0    -1  
$EndComp
$Comp
L crafting_labs:display_128x32_i2c U5
U 1 1 61D84AAA
P 8650 1900
F 0 "U5" H 9542 2315 50  0000 C CNN
F 1 "display_128x32_i2c" H 9542 2224 50  0000 C CNN
F 2 "crafting-labs:display_128x32_i2c" H 10400 1700 50  0001 C CNN
F 3 "" H 10400 1700 50  0001 C CNN
	1    8650 1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_OTG J1
U 1 1 61D85243
P 4550 3550
F 0 "J1" H 4607 4017 50  0000 C CNN
F 1 "USB_OTG" H 4607 3926 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 4700 3500 50  0001 C CNN
F 3 " ~" H 4700 3500 50  0001 C CNN
	1    4550 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 61D859B3
P 5250 2700
F 0 "C1" H 5132 2654 50  0000 R CNN
F 1 "CP 3.3" H 5132 2745 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D4.0mm_P1.50mm" H 5288 2550 50  0001 C CNN
F 3 "~" H 5250 2700 50  0001 C CNN
	1    5250 2700
	-1   0    0    1   
$EndComp
$Comp
L crafting_labs:MHZ19c U3
U 1 1 61D87816
P 6850 3150
F 0 "U3" H 6825 3615 50  0000 C CNN
F 1 "MHZ19c" H 6825 3524 50  0000 C CNN
F 2 "crafting-labs:MHZ19c" H 6850 3150 50  0001 C CNN
F 3 "" H 6850 3150 50  0001 C CNN
	1    6850 3150
	1    0    0    -1  
$EndComp
$Comp
L crafting_labs:ws2812_strip L1
U 1 1 61D87CC4
P 8900 4200
F 0 "L1" H 8983 4687 60  0000 C CNN
F 1 "ws2812_strip" H 8983 4581 60  0000 C CNN
F 2 "crafting-labs:ws2812_strip" H 9100 3800 60  0001 C CNN
F 3 "" H 9100 3800 60  0000 C CNN
	1    8900 4200
	1    0    0    -1  
$EndComp
$Comp
L crafting_labs:esp-dev-connector U2
U 1 1 61D8A16A
P 5950 4600
F 0 "U2" H 5967 4185 50  0000 C CNN
F 1 "esp-dev-connector" H 5967 4276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Horizontal" H 5950 4600 50  0001 C CNN
F 3 "" H 5950 4600 50  0001 C CNN
	1    5950 4600
	-1   0    0    1   
$EndComp
Text GLabel 4950 2700 2    50   Input ~ 0
5V
Text GLabel 5500 2850 2    50   Input ~ 0
3.3V
Text GLabel 5500 2550 2    50   Input ~ 0
GND
Text GLabel 4550 4150 3    50   Input ~ 0
GND
NoConn ~ 4850 3550
NoConn ~ 4850 3650
NoConn ~ 4850 3750
NoConn ~ 4450 3950
Wire Wire Line
	4900 2550 5250 2550
Wire Wire Line
	4900 2700 4950 2700
Text GLabel 6300 4800 2    50   Input ~ 0
GND
Text GLabel 7000 4400 2    50   Input ~ 0
RST
Text GLabel 7000 4500 2    50   Input ~ 0
FLASH
Text GLabel 6300 4600 2    50   Input ~ 0
TX
Text GLabel 6300 4700 2    50   Input ~ 0
RX
Wire Wire Line
	6200 4600 6300 4600
Wire Wire Line
	6200 4700 6300 4700
Wire Wire Line
	6200 4800 6300 4800
Text GLabel 7300 950  2    50   Input ~ 0
TX
Text GLabel 7300 1050 2    50   Input ~ 0
RX
Text GLabel 7300 1350 2    50   Input ~ 0
FLASH
Text GLabel 7300 1650 2    50   Input ~ 0
GND
Text GLabel 6050 1650 0    50   Input ~ 0
3.3V
Text GLabel 6050 950  0    50   Input ~ 0
RST
Text GLabel 8550 1950 0    50   Input ~ 0
5V
Text GLabel 8550 2050 0    50   Input ~ 0
GND
Text GLabel 8550 1750 0    50   Input ~ 0
SDA
Text GLabel 8550 1850 0    50   Input ~ 0
SCK
Text GLabel 7300 1250 2    50   Input ~ 0
SDA
Text GLabel 7300 1150 2    50   Input ~ 0
SCK
Text GLabel 6050 1150 0    50   Input ~ 0
3.3V
Wire Wire Line
	6050 950  6150 950 
Wire Wire Line
	6050 1150 6150 1150
Wire Wire Line
	6050 1650 6150 1650
Wire Wire Line
	7200 1650 7300 1650
Wire Wire Line
	7200 1350 7300 1350
Wire Wire Line
	7200 1250 7300 1250
Wire Wire Line
	7200 1150 7300 1150
Wire Wire Line
	7200 1050 7300 1050
Wire Wire Line
	7200 950  7300 950 
Wire Wire Line
	8550 1750 8650 1750
Wire Wire Line
	8550 1850 8650 1850
Wire Wire Line
	8550 1950 8650 1950
Wire Wire Line
	8550 2050 8650 2050
NoConn ~ 6400 3050
NoConn ~ 6400 3150
NoConn ~ 7250 3050
NoConn ~ 7250 2950
NoConn ~ 7250 3350
Text GLabel 5050 3350 2    50   Input ~ 0
5V
Text GLabel 6250 3350 0    50   Input ~ 0
5V
Text GLabel 7500 3150 2    50   Input ~ 0
TX1
Text GLabel 7500 3250 2    50   Input ~ 0
RX1
Text GLabel 6050 1550 0    50   Input ~ 0
TX1
Text GLabel 6050 1450 0    50   Input ~ 0
RX1
Wire Wire Line
	7250 3150 7500 3150
Wire Wire Line
	7250 3250 7500 3250
Wire Wire Line
	6250 3350 6400 3350
Wire Wire Line
	6050 1550 6150 1550
Wire Wire Line
	6150 1450 6050 1450
Text GLabel 9250 4150 2    50   Input ~ 0
LED_DATA
Text GLabel 7300 1550 2    50   Input ~ 0
LED_DATA
Text GLabel 9850 4000 2    50   Input ~ 0
GND
Text GLabel 4350 1500 0    50   Input ~ 0
GND
NoConn ~ 6150 1050
Wire Wire Line
	6400 3250 6250 3250
Wire Wire Line
	4850 3350 5050 3350
Text GLabel 4700 4700 0    50   Input ~ 0
GND
$Comp
L power:GND #PWR0101
U 1 1 61DD489D
P 4850 4700
F 0 "#PWR0101" H 4850 4450 50  0001 C CNN
F 1 "GND" V 4855 4572 50  0000 R CNN
F 2 "" H 4850 4700 50  0001 C CNN
F 3 "" H 4850 4700 50  0001 C CNN
	1    4850 4700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4550 3950 4550 4150
Text GLabel 4700 5000 0    50   Input ~ 0
3.3V
$Comp
L power:+3.3V #PWR0103
U 1 1 61DD6789
P 4850 5000
F 0 "#PWR0103" H 4850 4850 50  0001 C CNN
F 1 "+3.3V" V 4865 5128 50  0000 L CNN
F 2 "" H 4850 5000 50  0001 C CNN
F 3 "" H 4850 5000 50  0001 C CNN
	1    4850 5000
	0    1    1    0   
$EndComp
Text GLabel 6250 3250 0    50   Input ~ 0
GND
$Comp
L power:+5V #PWR0105
U 1 1 61DDD268
P 4100 4700
F 0 "#PWR0105" H 4100 4550 50  0001 C CNN
F 1 "+5V" V 4115 4828 50  0000 L CNN
F 2 "" H 4100 4700 50  0001 C CNN
F 3 "" H 4100 4700 50  0001 C CNN
	1    4100 4700
	0    1    1    0   
$EndComp
Text GLabel 3950 4700 0    50   Input ~ 0
5V
Wire Wire Line
	3950 4700 4050 4700
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 61DDFA9C
P 4050 4700
F 0 "#FLG0101" H 4050 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 4050 4873 50  0000 C CNN
F 2 "" H 4050 4700 50  0001 C CNN
F 3 "~" H 4050 4700 50  0001 C CNN
	1    4050 4700
	1    0    0    -1  
$EndComp
Connection ~ 4050 4700
Wire Wire Line
	4050 4700 4100 4700
Wire Wire Line
	4700 4700 4800 4700
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 61DE10E2
P 4800 4700
F 0 "#FLG0102" H 4800 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 4800 4873 50  0000 C CNN
F 2 "" H 4800 4700 50  0001 C CNN
F 3 "~" H 4800 4700 50  0001 C CNN
	1    4800 4700
	1    0    0    -1  
$EndComp
Connection ~ 4800 4700
Wire Wire Line
	4800 4700 4850 4700
Text GLabel 9850 4450 2    50   Input ~ 0
5V
$Comp
L Switch:SW_SPST SW2
U 1 1 61E00A03
P 4800 1500
F 0 "SW2" H 4800 1735 50  0000 C CNN
F 1 "B_2" H 4800 1644 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 4800 1500 50  0001 C CNN
F 3 "~" H 4800 1500 50  0001 C CNN
	1    4800 1500
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 61E00D75
P 4800 1150
F 0 "SW1" H 4800 1385 50  0000 C CNN
F 1 "B_1" H 4800 1294 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 4800 1150 50  0001 C CNN
F 3 "~" H 4800 1150 50  0001 C CNN
	1    4800 1150
	1    0    0    -1  
$EndComp
Connection ~ 4600 1500
Wire Wire Line
	4600 1500 4600 1150
Text GLabel 5200 1150 2    50   Input ~ 0
B_1
Text GLabel 5200 1500 2    50   Input ~ 0
B_2
Wire Wire Line
	5000 1150 5050 1150
Wire Wire Line
	5000 1500 5050 1500
Text GLabel 6050 1250 0    50   Input ~ 0
B_1
Text GLabel 6050 1350 0    50   Input ~ 0
B_2
Wire Wire Line
	6050 1250 6150 1250
Wire Wire Line
	6050 1350 6150 1350
Connection ~ 5250 2850
Wire Wire Line
	5250 2850 4900 2850
Connection ~ 5250 2550
Wire Wire Line
	5250 2850 5500 2850
Wire Wire Line
	5250 2550 5500 2550
$Comp
L Switch:SW_SPST SW4
U 1 1 61E157E4
P 6800 4750
F 0 "SW4" V 6754 4848 50  0000 L CNN
F 1 "FLASH/B_SCR" V 6845 4848 50  0000 L CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 6800 4750 50  0001 C CNN
F 3 "~" H 6800 4750 50  0001 C CNN
	1    6800 4750
	0    1    1    0   
$EndComp
Text GLabel 6800 5050 3    50   Input ~ 0
GND
Wire Wire Line
	6200 4500 6800 4500
Wire Wire Line
	6800 4500 6800 4550
Connection ~ 6800 4500
Wire Wire Line
	6800 4500 7000 4500
Wire Wire Line
	6800 4950 6800 5050
$Comp
L Switch:SW_SPST SW3
U 1 1 61E34D62
P 6800 4150
F 0 "SW3" V 6846 4062 50  0000 R CNN
F 1 "RESET" V 6755 4062 50  0000 R CNN
F 2 "Button_Switch_SMD:SW_SPST_TL3305A" H 6800 4150 50  0001 C CNN
F 3 "~" H 6800 4150 50  0001 C CNN
	1    6800 4150
	0    -1   -1   0   
$EndComp
Text GLabel 6800 3850 1    50   Input ~ 0
GND
Wire Wire Line
	6200 4400 6800 4400
Wire Wire Line
	6800 4400 6800 4350
Connection ~ 6800 4400
Wire Wire Line
	6800 4400 7000 4400
Wire Wire Line
	6800 3950 6800 3850
NoConn ~ 7200 1450
$Comp
L Device:R R2
U 1 1 61E45100
P 5050 1650
F 0 "R2" H 5120 1696 50  0000 L CNN
F 1 "R" H 5120 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4980 1650 50  0001 C CNN
F 3 "~" H 5050 1650 50  0001 C CNN
	1    5050 1650
	1    0    0    -1  
$EndComp
Connection ~ 5050 1500
Wire Wire Line
	5050 1500 5200 1500
$Comp
L Device:R R1
U 1 1 61E45A21
P 5050 1000
F 0 "R1" H 5120 1046 50  0000 L CNN
F 1 "R" H 5120 955 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 4980 1000 50  0001 C CNN
F 3 "~" H 5050 1000 50  0001 C CNN
	1    5050 1000
	1    0    0    -1  
$EndComp
Connection ~ 5050 1150
Wire Wire Line
	5050 1150 5200 1150
Text GLabel 5050 800  1    50   Input ~ 0
3.3V
Text GLabel 5050 1850 3    50   Input ~ 0
3.3V
Wire Wire Line
	5050 800  5050 850 
Wire Wire Line
	5050 1800 5050 1850
$Comp
L Device:CP C2
U 1 1 61E6D8D0
P 9800 4250
F 0 "C2" H 9682 4204 50  0000 R CNN
F 1 "CP 5v" H 9682 4295 50  0000 R CNN
F 2 "Capacitor_THT:CP_Radial_D8.0mm_P2.50mm" H 9838 4100 50  0001 C CNN
F 3 "~" H 9800 4250 50  0001 C CNN
	1    9800 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	9200 4000 9800 4000
Wire Wire Line
	9800 4000 9800 4100
Connection ~ 9800 4000
Wire Wire Line
	9800 4000 9850 4000
Wire Wire Line
	9200 4450 9800 4450
Wire Wire Line
	9800 4400 9800 4450
Connection ~ 9800 4450
Wire Wire Line
	9800 4450 9850 4450
Wire Wire Line
	7200 1550 7300 1550
Wire Wire Line
	4700 5000 4850 5000
Wire Wire Line
	9200 4150 9250 4150
$Comp
L Mechanical:MountingHole H1
U 1 1 61E9CD8F
P 2300 1000
F 0 "H1" H 2400 1046 50  0000 L CNN
F 1 "MountingHole" H 2400 955 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2300 1000 50  0001 C CNN
F 3 "~" H 2300 1000 50  0001 C CNN
	1    2300 1000
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 61E9D5CD
P 2300 1200
F 0 "H2" H 2400 1246 50  0000 L CNN
F 1 "MountingHole" H 2400 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2300 1200 50  0001 C CNN
F 3 "~" H 2300 1200 50  0001 C CNN
	1    2300 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 61E9D8F0
P 2300 1400
F 0 "H3" H 2400 1446 50  0000 L CNN
F 1 "MountingHole" H 2400 1355 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2300 1400 50  0001 C CNN
F 3 "~" H 2300 1400 50  0001 C CNN
	1    2300 1400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 61E9DC40
P 2300 1600
F 0 "H4" H 2400 1646 50  0000 L CNN
F 1 "MountingHole" H 2400 1555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 2300 1600 50  0001 C CNN
F 3 "~" H 2300 1600 50  0001 C CNN
	1    2300 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 1500 4600 1500
$EndSCHEMATC

# CO2-sensor

A CO2 device sensor based on the MH Z19c frm Winsen ([datasheet](https://www.winsen-sensor.com/d/files/infrared-gas-sensor/mh-z19c-pins-type-co2-manual-ver1_0.pdf)) and an esp8266 with a small oled display screen and some ws2812b leds.

![co2 sensor](images/co2.1024px.jpg)

## Features

* displays the current read of the sensor in ppm
* light green (<800ppm), yellow (<1000ppm) or red (>1200ppm)
* allows calibration
Note: even if the esp8266 has wifi capabilities, they are currently unused and the wifi is off.

### Possible future features

* allow the use of autocalibration capability of the sensor
* use wifi to upload data somewhere?

## Calibration

The sensor is factory calibrated but might require recalibration from time to time, specially if there are a lot of variation in temperature and humidity.

Calibration is a lenghty, but automatic process, it'll take around 40min.

Steps:
* find a very well ventilated (possibly outside) with no direct sunlight and protected from weather
* push button 1
* push button 1 again with 5 seconds
* place the sensor in the well ventilated place you found
* wait for 40min. Once the process is done, the sensors will display a message saying so and leds will be blue.
* push the button 1 again to resume normal operation
 

# Licence

All code and design on this repository is placed under the Hippocratic 2.1 licence. See details in [LICENCE.md](LICENCE.md).

You can read more about that licence on [the website firstdonoharm.dev](https://firstdonoharm.dev/).
